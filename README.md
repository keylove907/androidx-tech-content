# androidx.tech Curated Content

This repository holds YAML+Markdown files with the hand-assembled content
for [androidx.tech](https://androidx.tech), the AndroidX library resource
site.

For example, [this file's information](https://gitlab.com/commonsguy/androidx-tech-content/blob/master/androidx.appcompat/appcompat/info.yaml)
winds up on [this androidx.tech page](https://androidx.tech/artifacts/appcompat/appcompat/).

## Contributing Content

If you have resources that you feel are relevant for androidx.tech, send them along!

### Easy: Email

Send an email [to CommonsWare](mailto:androidx-tech@commonsware.com) with the links.
Assuming CommonsWare agrees that they are good resources for the site, CommonsWare
will add them to the associated content file and get them published.

### Slightly Harder: File an Issue

The repo's [issue tracker](https://gitlab.com/commonsguy/androidx-tech-content/issues)
is available for you to report problems with the site or to contribute new content.
However, you will need a GitLab account to do that.

### Hardest: Issue a Merge Request

You could fork the repo and publish a merge request (the GitLab equivalent of a
GitHub pull request). You will need to follow the overall YAML+Markdown structure
of the files for your merge to be accepted.
[This file](https://gitlab.com/commonsguy/androidx-tech-content/blob/master/androidx.appcompat/appcompat/info.yaml)
is a good example of how the content is structured.

#### Top-Level Keys

`docs` is for official documentation. Mostly this is for stuff on `developer.android.com`,
but official Google Medium posts could go here as well.

`samples` is for sample projects that use the artifact associated with this YAML.

`articles` is for anything else, including blog/Medium posts, videos, and so on.

#### Key Content

Each of those keys contains a list of name-value pairs. In YAML, that means:

- Each element in the list is started by a `-` indented two spaces

- Each name/value pair is indented four spaces under that `-`

The keys for those name/value pairs are:

- `title`

- `url`

- `description` (optional, if the title is insufficent)

For items in the `articles` key, please include the publication date of the content
in the title (e.g., `(2016-01-21)`).

#### Explanatory Material

Below the YAML is a Markdown block, indented four spaces, that provides additional
information about the contents and role of this artifact.

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.